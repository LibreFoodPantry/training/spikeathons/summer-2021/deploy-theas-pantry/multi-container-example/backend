# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [3.1.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.1.0...v3.1.1) (2021-07-21)

## [3.1.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.1...v3.1.0) (2021-07-21)


### Features

* updated various files ([5b75c0c](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/5b75c0c0458a422d8ff4536f0dcadd713e34ed32))

### [3.0.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.0...v3.0.1) (2021-07-21)


### Bug Fixes

* fixed container image link ([679e8d9](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/679e8d98828e5d2df727b6790d15b848f4f6d2f2))
* updated entrypoint bashscript ([2f7fd48](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/2f7fd482f5c81dc0aa4e1d0551d7efccbb1e8158))

### [3.0.9](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.8...v3.0.9) (2021-07-21)

### [3.0.8](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.7...v3.0.8) (2021-07-21)

### [3.0.7](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.6...v3.0.7) (2021-07-21)

### [3.0.6](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.5...v3.0.6) (2021-07-21)

### [3.0.5](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.4...v3.0.5) (2021-07-21)

### [3.0.4](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.3...v3.0.4) (2021-07-21)

### [3.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.2...v3.0.3) (2021-07-21)

### [3.0.2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.1...v3.0.2) (2021-07-21)

### [3.0.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.0...v3.0.1) (2021-07-21)


### Bug Fixes

* fixed container image link ([679e8d9](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/679e8d98828e5d2df727b6790d15b848f4f6d2f2))
* updated entrypoint bashscript ([2f7fd48](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/2f7fd482f5c81dc0aa4e1d0551d7efccbb1e8158))

### [3.0.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.0...v3.0.1) (2021-07-21)


### Bug Fixes

* fixed container image link ([679e8d9](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/679e8d98828e5d2df727b6790d15b848f4f6d2f2))
* updated entrypoint bashscript ([2f7fd48](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/2f7fd482f5c81dc0aa4e1d0551d7efccbb1e8158))

### [3.0.2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.1...v3.0.2) (2021-07-21)

### [3.0.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v3.0.0...v3.0.1) (2021-07-21)


### Bug Fixes

* updated entrypoint bashscript ([2f7fd48](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/2f7fd482f5c81dc0aa4e1d0551d7efccbb1e8158))

## [3.0.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.1.0...v3.0.0) (2021-07-21)


### ⚠ BREAKING CHANGES

* added Dockerfile

### Features

* added Dockerfile ([31aa5ea](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/31aa5ea4911ba0fd3adefa361c6cf1b50fdc249f))


### Bug Fixes

* updated entrypoint script ([963c197](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/963c197edfdc63075d4079eeeef540f8ec29b7e6))

### [2.1.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.1.0...v2.1.1) (2021-07-21)


### Bug Fixes

* updated entrypoint script ([963c197](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/963c197edfdc63075d4079eeeef540f8ec29b7e6))

## [2.1.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.3...v2.1.0) (2021-07-21)


### Features

* various changes in docker-compose and release-image yaml files ([7a337ad](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/7a337ada3b57b92a4f2ee97413234a8e278b6e12))


### Bug Fixes

* Updated release-image yml file to use specific image for release ([d25cf31](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/d25cf31b65d194ea817e6b4038b20472ae694b46))

### [2.0.4](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.3...v2.0.4) (2021-07-21)


### Bug Fixes

* Updated release-image yml file to use specific image for release ([d25cf31](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/d25cf31b65d194ea817e6b4038b20472ae694b46))

### [2.0.4](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.3...v2.0.4) (2021-07-21)


### Bug Fixes

* Updated release-image yml file to use specific image for release ([d25cf31](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/d25cf31b65d194ea817e6b4038b20472ae694b46))

### [2.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.2...v2.0.3) (2021-07-21)


### Bug Fixes

* updated docker-compose file to include IMAGES_TO_RELEASE parameter ([ec5ee83](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/ec5ee8337f0c69e8e505d5d8c1ce693dd8540e72))
* updated docker-compose file to include IMAGES_TO_RELEASE parameter ([59dab07](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/59dab07013812c95882e10eae44154e90394e69e))

### [2.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.2...v2.0.3) (2021-07-21)


### Bug Fixes

* updated docker-compose file to include IMAGES_TO_RELEASE parameter ([59dab07](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/59dab07013812c95882e10eae44154e90394e69e))

### [2.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.2...v2.0.3) (2021-07-21)

### [2.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.2...v2.0.3) (2021-07-21)

### [2.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.2...v2.0.3) (2021-07-21)

### [2.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.2...v2.0.3) (2021-07-21)

### [2.0.3](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.2...v2.0.3) (2021-07-21)

### [2.0.2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.1...v2.0.2) (2021-07-21)

### [2.0.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v2.0.0...v2.0.1) (2021-07-21)


### Bug Fixes

* updated entrypoint script ([3e09632](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/3e09632ba7eff138dd11b3a08ac3d81ace303e04))

## [2.0.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v1.1.1...v2.0.0) (2021-07-21)


### ⚠ BREAKING CHANGES

* added release-image yml file

### Features

* added release-image yml file ([d6b4c46](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/d6b4c46acd5e4853d6c2b8f43861a59255466f03))

### [1.1.2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v1.1.1...v1.1.2) (2021-07-21)

### [1.1.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v1.1.0...v1.1.1) (2021-07-21)


### Bug Fixes

* updated the entrypoint bash script ([ba10dc8](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/ba10dc8c7e5f67f476a15f570a92d64ba8d1275e))

## [1.1.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/compare/v1.0.0...v1.1.0) (2021-07-21)


### Features

* added a text file ([1848e26](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/multi-container-example/backend/commit/1848e263e5cdb6c0b14f17784491adfb66d7b399))

## 1.0.0 (2021-07-21)

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
